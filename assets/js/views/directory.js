var DirectoryView = Backbone.View.extend({
	el: $('#contacts'),
	events : {
		'change #filter select' : 'setFilter',
		'click #add' : 'addContact',
		'click #showForm' : 'showForm'
	},

	initialize : function(options){
		this.contacts = options.contacts;
		this.contactsRouter = options.contactsRouter;

		this.collection = new Directory(this.contacts);

		this.render();
		this.$el.find("#filter").append(this.createSelect());

		this.on('change:filterType', this.filterByType, this);
		//change view when reset is called
		this.collection.on('reset', this.render, this);
		this.collection.on('add', this.renderContact, this);
		this.collection.on('remove', this.removeContact, this);
	},

	render : function(){
		var contactView = new ContactView();
		this.$el.find(contactView.tagName).remove();
		
        _.each(this.collection.models, function (item) {
            this.renderContact(item);
        }, this);
        
	},
	renderContact : function(item){
		var contactView = new ContactView({
			model : item,
			contacts : this.contacts
		});

		$('#contacts').append(contactView.render(this).el);
	},
	getTypes : function(collection){
		if(typeof collection !== "undefined")
		{
			return _.uniq(collection.pluck('type'), false, function(type){
				return type.toLowerCase();
			})
		} else { 
			return _.uniq(this.collection.pluck('type'), false, function(type){
				return type.toLowerCase();
			})
		}

	},
	getAllTypes : function(full){
		var fullDirectory = new Directory(this.contacts);
		return full ? this.getTypes(fullDirectory) : this.getTypes() ;
	},
	createSelect : function(){
		//var filter = this.$el.find('#contacts');
		var select = $('<select/>', {
			html: '<option value="all">All</option>'
		});
		_.each(this.getAllTypes(), function(item) {
			var option = $('<option/>', {
				value : item.toLowerCase(),
				text : item.toLowerCase()
			}).appendTo(select);

		});
		return select;
	},
	setFilter : function(evt){
		this.filterType = evt.currentTarget.value;
		this.trigger("change:filterType");
	}, 
	filterByType : function(){
		if(this.filterType === "all") {
			this.collection.reset(this.contacts);

			this.contactsRouter.navigate("filter/all", {trigger : false});

		} else {
			this.collection.reset(this.contacts, {
				silent : false // notify router
			});

			var filterType = this.filterType;
			var filtered = _.filter(this.collection.models, function(item){
				return item.get('type').toLowerCase() === filterType;
			});
			this.collection.reset(filtered);
			this.contactsRouter.navigate("filter/" + filterType, {trigger : false});
		}
		this.$el.find('#filter select').val(this.filterType);
	},
	addContact : function(evt){
		evt.preventDefault();
		var newModel = {};
		$('#addContact').children('input').each(function(key, element){
			if($(element).val() !== "") {
				newModel[element.id] = $(element).val();
			}
		});
		this.contacts.push(newModel);
		if(_.indexOf(this.getAllTypes(), newModel.type) === -1){
			this.collection.add(new Contact(newModel));
			this.updateSelect();
			console.log('Added option: '+ newModel.type);		
					//fix select options
		} else {
			this.updateSelect();
			this.collection.add(new Contact(newModel));
		}
		console.log('Added model: '+ newModel.name);	
	},
	removeContact : function(removedModel){
		var removed = removedModel.attributes;
		if(removed.photo === 'assets/img/placeholder.png'){
			delete removed.photo;
		}
		var _this = this;
		_.each(this.contacts, function(contact){
			if(_.isEqual(contact, removed)){
				_this.contacts.splice(_.indexOf(_this.contacts, contact), 1);
			}
		});
	},
	showForm : function () {
		this.$el.find('#addContact').slideToggle();
	},
	updateSelect : function (){
		this.$el.find('#filter')
			.find('select')
				.remove()
			.end()
			.append(this.createSelect());
					
	}
});