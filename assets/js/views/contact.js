var ContactView = Backbone.View.extend({
	//container (el) alternative
	tagName : 'article',
	//automatically added to the container
	className : 'contact-container',
	//stored cache reference to be used in jQuery
	template : $('#contactTemplate').html(),
	editTemplate : _.template($('#contactEditTemplate').html()),

	events : {
		'click button.delete' : 'deleteContact',
		'click button.edit' : 'editContact',
		'click button.save' : 'saveContact',
		'click button.cancel' : 'cancelUpdate',
		'change select.type' : 'addType'
	},
	initialize : function(options){
		this.contacts = window.all_contacts;
	},
	render : function(directoryView){
		var tmpl = _.template(this.template);
		this.directoryView = directoryView;
		// console.log(directoryView);
		$(this.el).html(tmpl(this.model.toJSON()));
		return this;
	},
	deleteContact : function() {
		var removedType = this.model.get('type').toLowerCase();

		this.model.destroy();
		this.remove();

		if(_.indexOf(this.directoryView.getAllTypes(), removedType) === -1) {
			this.directoryView.$el.find('#filter select')
			.children('[value="' + removedType + '"]')
			.remove();
			// this.directoryView.updateSelect();

			console.log('Removed option: '+ removedType);
		}
		console.log('Removed model: '+ this.model.get('name'));
	},
	editContact : function(){
		this.$el.html(this.editTemplate(this.model.toJSON()));
		var newOpt = $('<option/>', {
			html: '<em>Add new</em>',
			value: 'addType'
		});
		this.select = this.directoryView.createSelect()
						.addClass('type')
						.val(this.$el.find('#type').val())
						.append(newOpt)
						.insertAfter(this.$el.find('.name'));

		this.$el.find('input[type="hidden"]').remove();
	},
	saveContact : function(e){
		e.preventDefault();

		var formData = {};
		
		console.log(this.model.previousAttributes());

		$(e.target).closest("form")
			.find(":input")
			.add(".photo")
			.not(":button").each(function(){
				var el = $(this);
				formData[el.attr('class')] = el.val();
			});
			console.log("saveContact: FormData => ");
			console.log(formData);

		if(formData.photo === ""){
			delete formData.photo;
		}

		this.model.set(formData);
		this.render(this.directoryView);
		var prev = this.model.previousAttributes();

		if(prev.photo === this.model.defaults.photo){
			console.log('delete photo');
			delete prev.photo;
		}
		var _this = this;
		_.each(this.contacts, function(contact){
			if(_.isEqual(contact, prev)){
				_this.contacts.splice(_.indexOf(_this.contacts, contact), 1, formData);
				console.log('success');
			}
			console.log('fail');
		});
		this.directoryView.updateSelect();
		// console.log(this.contacts);
		// console.log(window.all_contacts);

	},
	cancelUpdate : function(e){
		e.preventDefault();
		this.render(this.directoryView);
	},
	addType : function(){
		if(this.select.val() === "addType"){
			this.select.remove();
			
			$('<input />', {
				'class' : 'type',
				// 'placeholder' : 'test'
			}).insertAfter(this.$el.find(".name"));

		}
	}
});