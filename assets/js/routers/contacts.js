var ContactsRouter = Backbone.Router.extend({
	routes : {
		'filter/:type' : 'urlFilter'
	},
	initialize : function(){
		this.directory = new DirectoryView({
			contacts : window.all_contacts,
			contactsRouter : this
		});
	},
	urlFilter : function(type){
		this.directory.filterType = type;
		this.directory.trigger('change:filterType');
	}
});