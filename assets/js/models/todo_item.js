var TodoItem = Backbone.Model.extend({});
var todoItem = new TodoItem ({
	description: 'Pick up milk',
	status: 'incomplete',
	id: 1
});

todoItem.get('description');
todoItem.set({status: 'complete'});
//todoItem.save();

var TodoView = Backbone.View.extend({
	render : function(){
		var html = '<h3>' +  this.model.get('description') + '</h3>';
		$(this.el).html(html);
	}
});

var todoView = new TodoView({
	model: todoItem
});

todoView.render();
console.log(todoView.el);