// var Contact = Backbone.Model.extend({

// 	//Configure default values for any attribute of the model
// 	defaults : {
// 		photo : '/img/placeholder.png'
// 	}
// 	//invoked by backbone when model is initialized (constructor)
// });

    var Contact = Backbone.Model.extend({
        defaults: {
            photo : "assets/img/placeholder.png",
            name : "",
            address : "",
            tel : "",
            email : "",
            type : ""
        }
    });
